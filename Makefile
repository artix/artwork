Version=1

PREFIX ?= /usr

ICONS = \
	$(wildcard icons/*.svg) \
	$(wildcard icons/*.png)

BACKGROUNDS = \
	$(wildcard backgrounds/*.jpg) \
	$(wildcard backgrounds/*.png)

GRUB_THEME = \
	$(wildcard artix-grub-theme/*.png) \
	artix-grub-theme/theme.txt \
	artix-grub-theme/u_vga16_16.pf2

GRUB_ICONS = \
	$(wildcard artix-grub-theme/icons/*.png)

SDDM_THEME = \
	$(wildcard artix-breeze-sddm/*)

install_icons:
	install -dm0755 $(DESTDIR)$(PREFIX)/share/icons/artix
	install -m0644 ${ICONS} $(DESTDIR)$(PREFIX)/share/icons/artix

install_logo:
	install -dm0755 $(DESTDIR)$(PREFIX)/share/icons/hicolor/scalable/apps
	install -m0644 icons/logo.svg $(DESTDIR)$(PREFIX)/share/icons/hicolor/scalable/apps/artixlinux-logo.svg

install_backgrounds:
	install -dm0755 $(DESTDIR)$(PREFIX)/share/backgrounds
	install -m0755 ${BACKGROUNDS} $(DESTDIR)$(PREFIX)/share/backgrounds
	install -dm0755 $(DESTDIR)$(PREFIX)/share/gnome-background-properties
	install -m0755 backgrounds/artix.xml $(DESTDIR)$(PREFIX)/share/gnome-background-properties/artix.xml

install_grub_theme:
	install -dm0755 $(DESTDIR)$(PREFIX)/share/grub/themes/artix
	install -m0644 $(GRUB_THEME) $(DESTDIR)$(PREFIX)/share/grub/themes/artix

	install -dm0755 $(DESTDIR)$(PREFIX)/share/grub/themes/artix/icons
	install -m0644 $(GRUB_ICONS) $(DESTDIR)$(PREFIX)/share/grub/themes/artix/icons

install_sddm_theme:
	install -dm0755 $(DESTDIR)$(PREFIX)/share/sddm/themes/breeze
	install -m0644 $(SDDM_THEME) $(DESTDIR)$(PREFIX)/share/sddm/themes/breeze

install: install_icons install_logo install_backgrounds install_grub_theme install_sddm_theme

.PHONY: install install_icons install_logo install_backgrounds install_grub_theme install_sddm_theme
